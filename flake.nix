{
  description = "pkgs";

  outputs = { self, nixpkgs, mkNixpkgs }: { 
    strok = {
      praim = { djenereicyn = 6; spici = "indeks"; };
      spici = { "*" = "iuniksOrRysolvyr"; };
    };

    datom = mkNixpkgs.datom {
      inherit nixpkgs;
      config = {
        allowUnfree = true;
      };
    };

  };
}
